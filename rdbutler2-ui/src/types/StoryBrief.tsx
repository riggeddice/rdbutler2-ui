
export interface StoryBrief {
    storyUid: string;
    title: string;
    threads: string;
    motives: string;
    startDate: string;
    endDate: string;
    players: string;
  }
  