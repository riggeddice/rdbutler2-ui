export interface FactionBrief {
    name: string;
    intensity: number;
    link: string;
    shortDesc: string;
}