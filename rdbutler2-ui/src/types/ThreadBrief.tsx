
export interface ThreadBrief {
    threadUid: string,
    name: string,
    startDate: string,
    endDate: string,
    storyCount: number,
    players: string[],
    playerActors: string[],
    shortDesc: string
}
