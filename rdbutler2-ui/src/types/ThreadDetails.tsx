import {GeneralStoryBlock} from './GeneralStoryBlock'

export interface ThreadDetails {
  uid: string;
  name: string;
  shortDesc: string;
  fullDesc: string;
  spoilers: string;
  storyBlocks: GeneralStoryBlock[];
}