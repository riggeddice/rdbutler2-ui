import { ActorMerit } from "./ActorDetailsSpace/ActorMerit";
import { ActorToActorRelation } from "./ActorDetailsSpace/ActorToActorRelation";

export interface FactionDetails{
    factionId: string;
    name: string;
    shortdesc: string;
    body: string;
    merits: ActorMerit[];
    ftarRelations: ActorToActorRelation[];
    ftfrRelations: ActorToActorRelation[];
}