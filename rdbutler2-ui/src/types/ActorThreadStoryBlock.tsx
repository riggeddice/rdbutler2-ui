
export interface ActorThreadStoryBlock {
    originatingActor: string;
    storyUid: string;
    storyTitle: string;
    startDate: string;
    endDate: string;
    summary: string;
    targetActorDeed: string;
    targetActorProgressions: string[];
    allActorsPresent: string[];
}