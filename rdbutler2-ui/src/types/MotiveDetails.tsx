import { AcdeorPlus } from './AcdeorPlus';
import {GeneralStoryBlock} from './GeneralStoryBlock'

export interface MotiveDetails {
  uid: string;
  name: string;
  shortDesc: string;
  fullDesc: string;
  spoilers: string;
  storyBlocks: GeneralStoryBlock[];
  motiveAcdeors: AcdeorPlus[];
}