export interface GeneralStoryBlock {
    storyUid: string;
    storyTitle: string;
    startDate: string;
    endDate: string;
    threads: string[];
    motives: string[];
    summary: string;
    allActorsPresent: string[];
  }