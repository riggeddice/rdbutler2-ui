import { ActorMerit } from "./ActorDetailsSpace/ActorMerit";
import { ActorProgression } from "./ActorDetailsSpace/ActorProgression";
import { ActorToActorRelation } from "./ActorDetailsSpace/ActorToActorRelation";
import { FlashcardRecord } from "./ActorDetailsSpace/FlashcardRecord";

export type ActorDetails = {
    uid: string;
    name: string;
    mechver: string;
    factions: string[];     // probably not displayed because perma-empty XD
    owner: string;
    body: string;
    merits: ActorMerit[];
    progressions: ActorProgression[];
    actorToActorRelations: ActorToActorRelation[];
    actorToFactionRelations: ActorToActorRelation[];
    flashcardRecords: FlashcardRecord[];
    threads: string[];
  }
