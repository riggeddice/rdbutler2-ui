export interface LocationRecord {
    originatingStory: string;
    name: string;
    event: string;
    path: string;
    endDate: string;
}