export interface ActorBrief {
    name: string;
    mechver: string;
    intensity: number;
    link: string;
}