export interface ActorMerit
{
    originUid: string;
    actor: string;
    deed: string;
    threads: string;
    startDate: string;
    endDate: string;
}