export interface ActorProgression{
    originUid: string;
    actor: string;
    deed: string;
    endDate: string;      
}