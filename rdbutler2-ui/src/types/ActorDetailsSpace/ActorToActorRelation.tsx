export interface ActorToActorRelation{
    targetActor: string;
    relevantActor: string;
    intensity: number;
    storyUids: string[];
}