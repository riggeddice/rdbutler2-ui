export interface FlashcardRecord
{
    originStoryUid: string;
    body: string;
};