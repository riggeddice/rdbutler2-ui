export interface LocationTreeNode {
    title: string;
    path: string;
    children: LocationTreeNode[];
  }