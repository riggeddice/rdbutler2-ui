import { GeneralStoryBlock } from "./GeneralStoryBlock";
import { LocationRecord } from "./LocationRecord";

export interface SingleLocationDetails {
    name: string;
    path: string;
    relatedStories: string[];
    relatedThreads: string[];
    allActorsPresent: string[];
    firstStartDate: string;
    lastEndDate: string;
    usefulLocationRecords: LocationRecord[];
    storyBlocks: GeneralStoryBlock[];
}
