
export interface MotiveBrief {
    motiveUid: string,
    name: string,
    startDate: string,
    endDate: string,
    storyCount: number,
    players: string[],
    playerActors: string[],
    shortDesc: string
}
