export interface StoryDetails {
    storyUid: string;               
    storyTitle: string;             
    threads: string[];              
    motives: string[];              
    startDate: string;              
    endDate: string;                
    sequenceNumber: string;         
    previousStories: string[];      
    gms: string[];                  
    players: string[];              
    playerActors: string[];         
    allActors: string[];            
    summary: string;                
    body: string
}